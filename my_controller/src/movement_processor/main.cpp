# include "my_controller/movement_processor/movement_processor.h"

int main(int argc,char **argv)
{
  ros::init(argc, argv, "my_controller", ros::init_options::AnonymousName);
  my_controller::MovementProcessor obj(argc, argv);
  ros::spin();
  return 0;
}
